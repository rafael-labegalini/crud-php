<?php
require 'vendor/autoload.php';

$app = \DB1\Bootstrap::getInstance();
?>
<html>
<head>
	<title>php-crud</title>
	<link href="bootstrap/css/bootstrap.css" rel="stylesheet">
	<link href="bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
	<style type="text/css">
		body {
			padding-top: 40px;
			padding-bottom: 40px;
			background-color: #f5f5f5;
		}

		.form-signin {
			max-width: 500px;
			padding: 19px 29px 29px;
			margin: 0 auto 20px;
			background-color: #fff;
			border: 1px solid #e5e5e5;
			-webkit-border-radius: 5px;
			-moz-border-radius: 5px;
			border-radius: 5px;
			-webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
			-moz-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
			box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
		}

		.form-signin .form-signin-heading,
		.form-signin .checkbox {
			margin-bottom: 10px;
		}

		.form-signin input[type="text"],
		.form-signin input[type="password"] {
			font-size: 16px;
			height: auto;
			margin-bottom: 15px;
			padding: 7px 9px;
		}

	</style>

</head>
<body>


<div class="container">

	<?=$app->flashMessages()->render()?>

	<form class="form-signin" action="form-handle.php" method="post">
		<h2 class="form-signin-heading">Cadastre-se</h2>
		<input type="text" class="input-block-level" placeholder="Email address" name="email">
		<input type="password" class="input-block-level" placeholder="Password" name="password">


		<input type="submit" class="btn btn-large btn-primary" name="action" value="Salvar">
		<input type="submit" class="btn btn-large btn-primary" name="action" value="Remover">
		<input type="submit" class="btn btn-large btn-primary" name="action" value="Incluir">
	</form>

</div>
<!-- /container -->


</body>
</html>
<?php
/**
 * Created by PhpStorm.
 * User: rafaellabegalini
 * Date: 8/10/15
 * Time: 1:23 AM
 */

namespace DB1\Entity\Repository;


use DB1\Entity\User;
use Doctrine\ORM\EntityRepository;

class Users extends EntityRepository
{
	public function findByEmailAndPassword($email, $password)
	{
		$user = $this->findOneBy(['email' => $email]); /* @var $user User */
		if(null === $user){
			return null;
		}

		if(!password_verify($password, $user->getPassword())){
			return null;
		}

		return $user;
	}
}
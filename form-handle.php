<?php

require 'vendor/autoload.php';

$app = \DB1\Bootstrap::getInstance();

if(!$app->isPost()){
	//se não for via post não faz nada, apenas redireciona
	header('location: /');
	exit;
}

$action   = $app->getPost('action', null);
$email    = $app->getPost('email', null);
$password = $app->getPost('password', null);

if($action === 'Incluir'){
	$em = $app->getEntityManager();

	//primeiro verifica se não existe um usuário igual cadastrado
	$user = $em->getRepository(\DB1\Entity\User::class)->findOneBy(['email' => $email]);
	if(null !== $user){
		$app->flashMessages()->addErrorMessage("Já existe um usuário com o email '{$email}' cadastrado");
		header('location: /');
		exit;
	}

	$user = new \DB1\Entity\User();
	$user->setEmail($email);
	$user->setPassword($password);

	$em->persist($user);
	$em->flush();

	$app->flashMessages()->addSuccessMessage("Usuário inserido com sucesso");
	header('location: /');
	exit;

} else if($action === 'Salvar'){
	$em = $app->getEntityManager();

	//busca o registro com o email e senha passados
	$user = $em->getRepository(\DB1\Entity\User::class)->findOneBy(['email' => $email]); /* @var $user \DB1\Entity\User */
	if(null === $user){
		//se não achar nenhum usuário redireciona para a index com uma mensagem de erro
		$app->flashMessages()->addErrorMessage('Nenhum usuário encontrado com o email "'.$email.'"');
		header('location: /');
		exit;
	}

	$user->setEmail($email);
	$user->setPassword($password);

	$em->flush();

	$app->flashMessages()->addSuccessMessage("Usuário alterado com sucesso");
	header('location: /');
	exit;
} else if($action === 'Remover'){
	$em = $app->getEntityManager();

	$user = $em->getRepository(\DB1\Entity\User::class)->findByEmailAndPassword($email, $password);
	if(null === $user){
		$app->flashMessages()->addErrorMessage("Nenhum usuário encontrado com o email '{$email}'");
		header('location: /');
		exit;
	}

	$em->remove($user);
	$em->flush();

	$app->flashMessages()->addSuccessMessage("Usuário removido com sucesso");
	header('location: /');
	exit;
}